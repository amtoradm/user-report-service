package com.retelzy.report.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.retelzy.report.dto.EmployeeDto;
import com.retelzy.report.entity.Employee;

public class ExcelHelper {
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "First Name",
              "Last Name",
              "Date of Hire",
              "Citizenship",
              "Visa Category",
              "Expiration Date",
              "Earliest Renewal Date",
              "Latest Renewal Date",
              "Job Title",
              "Salary",
              "Client Name",
              "Employment Location",
              "Project End Date"
			};
	static String SHEET = "Employees";

	public static ByteArrayInputStream employeesToExcel(List<EmployeeDto> employee) {

		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheet = workbook.createSheet(SHEET);

			// Header
			Row headerRow = sheet.createRow(0);

			for (int col = 0; col < HEADERs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(HEADERs[col]);
			}

			int rowIdx = 1;
			for (EmployeeDto emp : employee) {
				Row row = sheet.createRow(rowIdx++);

				row.createCell(0).setCellValue(emp.getFirstName());
				row.createCell(1).setCellValue(emp.getLastName());
				row.createCell(2).setCellValue(emp.getDateofRehire());
				row.createCell(3).setCellValue(emp.getCountry());
				row.createCell(4).setCellValue(emp.getSalary());
				row.createCell(5).setCellValue(emp.getClientName());
				row.createCell(6).setCellValue(emp.getEmploymentLocation());
				row.createCell(7).setCellValue(emp.getProjectEndDate());
				row.createCell(8).setCellValue(emp.getTitleOfEmpOrAuthorizedRepresentative());
				row.createCell(9).setCellValue(emp.getVisaCategory());
				row.createCell(10).setCellValue(emp.getVisaEarliestRenewalDate());
				row.createCell(11).setCellValue(emp.getVisaLatestRenewalDate());
				row.createCell(12).setCellValue(emp.getVisaExpDate());
			}

			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
	}

}
