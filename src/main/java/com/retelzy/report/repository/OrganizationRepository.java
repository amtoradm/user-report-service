package com.retelzy.report.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.report.entity.Organization;


@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

	@Query(value = "select * from organization where user_id=?1", nativeQuery = true)
	Organization getOrganization(Long id);

}
