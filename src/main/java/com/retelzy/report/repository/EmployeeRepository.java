package com.retelzy.report.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.retelzy.report.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {


	@Query(value = "select * from employee where user_id=?1", nativeQuery = true)
	Employee getEmployee(Long id);

}
