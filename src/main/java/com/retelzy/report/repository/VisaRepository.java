package com.retelzy.report.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.retelzy.report.entity.Visa;

@Repository
public interface VisaRepository extends JpaRepository<Visa, Long> {

	@Query(value = "select * from visa where user_id=?1", nativeQuery = true)
	Visa getVisa(Long userId);

}
