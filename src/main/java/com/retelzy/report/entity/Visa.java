package com.retelzy.report.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Visa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long visaId;
    @Column(unique=true)
    private Long userId;
    private String isUSCitizen;
    private String isNonUSCitizen;
    private String isEmplawfulPermanentResident;
    private String isAuthorizedToWork;
    private String permAlienRegNo;
    private String alienAuthToWorkRegNo;
    private String alienAuthToWorkI94AdmiNo;
    private String alienAuthToWorkForeignPassportNum;
    private String alienAuthToWorkForeignPassportCountry;
    private Date authWorkExpDate;
    private String docTitle;
    private String docNumber;
    private Date docExpDate;
    private String visaCategory;
    private Date visaExpDate;
    private Date visaEarliestRenewalDate;
    private Date visaLatestRenewalDate;
    
}
