package com.retelzy.report.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long empId;
    @Column(unique=true)
    private Long userId;
    private String lastName;
    private String firstName;
    private String middleInitial;
    private String otherLastName;
    private Date dateOfBirth;
    private String gender;
    private String USSocSecNum;
    private String email;
    private Double telephoneNumber;
    private Double mobileNumber;
    private Double alternateMobileNumber;
}
