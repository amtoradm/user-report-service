package com.retelzy.report.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	private String email;

	@Column(length = 60)
	private String userName;

	@Column(length = 60)
	private String password;

	public String roleModel;
	public String roles;
	private boolean enabled = false;
}
