package com.retelzy.report.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {

    private Long userId;
    private String lastName;
    private String firstName;
    private Date dateofRehire;
    private String country;
    private String salary;
    private String clientName;
    private String employmentLocation;
    private Date projectEndDate;
    private String titleOfEmpOrAuthorizedRepresentative;
    private String visaCategory;
    private Date visaExpDate;
    private Date visaEarliestRenewalDate;
    private Date visaLatestRenewalDate;

    
}
