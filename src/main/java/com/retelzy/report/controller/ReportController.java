package com.retelzy.report.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.retelzy.report.dto.EmployeeDto;
import com.retelzy.report.entity.Employee;
import com.retelzy.report.service.ReportServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/excel")
public class ReportController {

	@Autowired
	ReportServiceImpl reportService;

	@GetMapping("/download")
	public ResponseEntity<?> getFile() {
		String filename = "ris_report.xlsx";
		InputStreamResource file = new InputStreamResource(reportService.load());

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}
	
	@GetMapping(value = "/getEmployee")
	public ResponseEntity<?> getEmployee() {
		List<EmployeeDto> employeeDto = reportService.getEmployeeInfo();
		if (employeeDto != null) {
			return new ResponseEntity<>(employeeDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/getEmployeeReportDetails")
	public ResponseEntity<?> getEmployeeReportDetails(@RequestParam("userId") Long userId) {
		List<EmployeeDto> employeeDto = reportService.getEmployeeReportDetails(userId);
		if (employeeDto != null) {
			return new ResponseEntity<>(employeeDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
