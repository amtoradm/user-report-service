package com.retelzy.report.service;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retelzy.report.dto.EmployeeDto;
import com.retelzy.report.entity.Employee;
import com.retelzy.report.entity.Organization;
import com.retelzy.report.entity.User;
import com.retelzy.report.entity.Visa;
import com.retelzy.report.helper.ExcelHelper;
import com.retelzy.report.repository.EmployeeRepository;
import com.retelzy.report.repository.OrganizationRepository;
import com.retelzy.report.repository.UserRepository;
import com.retelzy.report.repository.VisaRepository;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	VisaRepository visaRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;

	@Override
	public ByteArrayInputStream load() {
		List<EmployeeDto> employee = getEmployeeInfo();
		ByteArrayInputStream in = ExcelHelper.employeesToExcel(employee);
		return in;
	}

	@Override
	public List<EmployeeDto> getEmployeeInfo() {
		List<User> user = userRepository.getUsers();
		List<EmployeeDto> employeeDto =  new ArrayList<EmployeeDto>();
		Employee emp = new Employee();
		Visa visa = new Visa();
		Organization organization= new Organization();
		
		ListIterator<User> iterator = user.listIterator();
		while (iterator.hasNext())  
			{  
			User u = iterator.next();  
			EmployeeDto empDto = new EmployeeDto();
			System.out.println(u.getId()); 
			emp = employeeRepository.getEmployee(u.getId());
			

			organization = organizationRepository.getOrganization(u.getId());
			
			if(organization!=null) {
				empDto.setDateofRehire(organization.getDateofRehire());
				empDto.setCountry(organization.getCountry());
				empDto.setSalary(organization.getSalary());
				empDto.setClientName(organization.getClientName());
				empDto.setEmploymentLocation(organization.getEmploymentLocation());
				empDto.setProjectEndDate(organization.getProjectEndDate());
				empDto.setTitleOfEmpOrAuthorizedRepresentative(organization.getTitleOfEmpOrAuthorizedRepresentative());
			}

			visa= visaRepository.getVisa(u.getId());
			
			if(visa!=null) {
				empDto.setVisaCategory(visa.getVisaCategory());
				empDto.setVisaEarliestRenewalDate(visa.getVisaEarliestRenewalDate());
				empDto.setVisaLatestRenewalDate(visa.getVisaLatestRenewalDate());
				empDto.setVisaExpDate(visa.getVisaExpDate());
			}
			
			if(emp!=null) {
				empDto.setUserId(u.getId());
				empDto.setLastName(emp.getLastName());
				empDto.setFirstName(emp.getFirstName());
				employeeDto.add(empDto);
			}
			
			}  
		
		return employeeDto;
	}

	public List<EmployeeDto> getEmployeeReportDetails(Long userId) {
		List<User> user = userRepository.getUsers();
		List<EmployeeDto> employeeDto =  new ArrayList<EmployeeDto>();
		Employee emp = new Employee();
		Visa visa = new Visa();
		Organization organization= new Organization();
		
		ListIterator<User> iterator = user.listIterator();
		if (userId!=null)  
			{  
			EmployeeDto empDto = new EmployeeDto();
			emp = employeeRepository.getEmployee(userId);
			

			organization = organizationRepository.getOrganization(userId);
			
			if(organization!=null) {
				empDto.setDateofRehire(organization.getDateofRehire());
				empDto.setCountry(organization.getCountry());
				empDto.setSalary(organization.getSalary());
				empDto.setClientName(organization.getClientName());
				empDto.setEmploymentLocation(organization.getEmploymentLocation());
				empDto.setProjectEndDate(organization.getProjectEndDate());
				empDto.setTitleOfEmpOrAuthorizedRepresentative(organization.getTitleOfEmpOrAuthorizedRepresentative());
			}

			visa= visaRepository.getVisa(userId);
			
			if(visa!=null) {
				empDto.setVisaCategory(visa.getVisaCategory());
				empDto.setVisaEarliestRenewalDate(visa.getVisaEarliestRenewalDate());
				empDto.setVisaLatestRenewalDate(visa.getVisaLatestRenewalDate());
				empDto.setVisaExpDate(visa.getVisaExpDate());
			}
			
			if(emp!=null) {
				empDto.setUserId(userId);
				empDto.setLastName(emp.getLastName());
				empDto.setFirstName(emp.getFirstName());
				employeeDto.add(empDto);
			}
			
			}  
		
		return employeeDto;
	}
}
