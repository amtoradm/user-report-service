package com.retelzy.report.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.retelzy.report.dto.EmployeeDto;

public interface ReportService {
	
	ByteArrayInputStream load();
	
	List<EmployeeDto> getEmployeeInfo();
	
	List<EmployeeDto> getEmployeeReportDetails(Long userId);

}
